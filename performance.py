import random
from datetime import datetime
from collections import defaultdict


for N in [100,1000,10000]:
	print(f'\U00002728 {N} elementi nell\'array \U00002728')
	arr = [random.randint(1,10) for x in range(N)]

	start = datetime.now()

	print('\U000027A1 inizio test con cicli annidati')

	max_occ = 0
	max_occ_val = 0
	tmp_occ = 0

	for (i,num) in enumerate(arr):
		for j in range(N):
			if num == arr[j]:
				tmp_occ += 1
		if tmp_occ > max_occ:
			max_occ_val = num
			max_occ = tmp_occ
		tmp_occ = 0

	end = datetime.now()
	print(f'tempo di elaborazione: {(end - start).total_seconds() * 1000} millisecondi \U0000274C')

	start = datetime.now()

	print('\U000027A1 inizio test con ciclo singolo e hash table')

	map_occ = defaultdict(int)

	for num in arr:
		map_occ[num] += 1

	max_occ = 0
	max_occ_val = 0

	for k, v in map_occ.items():
		if v > max_occ:
			max_occ = v
			max_occ_val = k

	end = datetime.now()
	print(f'tempo di elaborazione: {(end - start).total_seconds() * 1000} millisecondi \U00002705')
	print()
